package com.linquan.feign.consumer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author linshimingyi
 * @package com.linquan.feign.consumer.model
 * @date 2019/10/28 22:37
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private String name;

    private Integer age;

}
