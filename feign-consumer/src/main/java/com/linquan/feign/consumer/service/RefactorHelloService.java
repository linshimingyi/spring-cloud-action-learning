package com.linquan.feign.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author linshimingyi
 * @package com.linquan.feign.consumer.service
 * @date 2019/11/9 16:52
 */
@FeignClient(value = "HELLO-SERVICE1")
public interface RefactorHelloService extends com.linquan.hello.service.api.service.HelloService {
}
