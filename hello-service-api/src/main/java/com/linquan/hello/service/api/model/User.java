package com.linquan.hello.service.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author linshimingyi
 * @package com.linquan.hello.service.api.model
 * @date 2019/11/9 16:26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private String name;

    private Integer age;

}
