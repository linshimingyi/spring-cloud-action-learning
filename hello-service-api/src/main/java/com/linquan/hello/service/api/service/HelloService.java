package com.linquan.hello.service.api.service;

import com.linquan.hello.service.api.model.User;
import org.springframework.web.bind.annotation.*;

/**
 * @author linshimingyi
 * @package com.linquan.hello.service.api.service
 * @date 2019/11/9 16:27
 */
@RequestMapping("/refactor")
public interface HelloService {

    @GetMapping(value = "/hello4")
    String hello(@RequestParam("name") String name);

    @GetMapping(value = "/hello5")
    User hello(@RequestHeader("name") String name, @RequestHeader("age") Integer age);

    @PostMapping(value = "/hello6")
    String hello(@RequestBody User user);
}
