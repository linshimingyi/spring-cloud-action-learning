package com.linquan.hello.service.web;

import com.linquan.hello.service.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author linshimingyi
 * @package com.linquan.hello.service.web
 * @date 2019/10/27 11:05
 */
@Slf4j
@RestController
public class HelloController {

    @Autowired
    private DiscoveryClient client;

    @GetMapping(value = "/hello")
    public String index() throws InterruptedException {
        List<String> services = client.getServices();
        if (services.size() > 0) {
            for (String service : services) {
                List<ServiceInstance> instances = client.getInstances(service);
                if (instances.size() > 0) {
                    for (ServiceInstance instance : instances) {
                        // 测试超时
                        int sleepTime = new Random().nextInt(3000);
                        log.info("sleepTime:" + sleepTime);
                        Thread.sleep(sleepTime);

                        log.info("/hello, host:" + instance.getHost() + ", service_id:" + instance.getServiceId());
                    }
                }
            }
        }
        return "Hello World!";
    }

    @GetMapping(value = "/hello1")
    public String hello(@RequestParam String name) {
        return "Hello" + name;
    }

    @GetMapping(value = "/hello2")
    public User hello(@RequestHeader String name, @RequestHeader Integer age) {
        return new User(name, age);
    }

    @PostMapping(value = "/hello3")
    public String hello(@RequestBody User user) {
        return "Hello" + user.getName() + ", " + user.getAge();
    }
}
