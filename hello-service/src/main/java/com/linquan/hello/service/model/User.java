package com.linquan.hello.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author linshimingyi
 * @package com.linquan.hello.service.model
 * @date 2019/10/27 11:06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private String name;

    private Integer age;

}
