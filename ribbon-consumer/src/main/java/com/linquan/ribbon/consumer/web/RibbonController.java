package com.linquan.ribbon.consumer.web;

import com.linquan.ribbon.consumer.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author linshimingyi
 * @package com.linquan.ribbon.consumer.web
 * @date 2019/10/27 11:30
 */
@RestController
public class RibbonController {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    private HelloService helloService;

    /*@GetMapping(value = "/ribbon-consumer")
    public String helloConsumer() {
        return restTemplate.getForEntity("http://HELLO-SERVICE/hello", String.class).getBody();
    }*/

    @GetMapping(value = "/ribbon-consumer")
    public String helloConsumer() {
        return helloService.helloService();
    }
}
